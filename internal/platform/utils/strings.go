package utils

import (
	"unicode"
	"unicode/utf8"
)

// StartsWithLetter returns true if the string starts with a letter.
func StartsWithLetter(s string) bool {
	r, _ := utf8.DecodeRuneInString(s)
	return unicode.IsLetter(r)
}
