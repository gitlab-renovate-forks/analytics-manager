package utils

import (
	"os"
	"testing"
)

func TestGetEnvFlag(t *testing.T) {
	_ = os.Setenv("TRUE_VALUE", "true")
	_ = os.Setenv("ONE_VALUE", "1")
	_ = os.Setenv("FALSE_VALUE", "false")
	_ = os.Setenv("OTHER_VALUE", "unknown")

	type args struct {
		key string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"GetEnvFlag returns true if env var is set to true", args{"TRUE_VALUE"}, true},
		{"GetEnvFlag returns true if env var is set to 1", args{"ONE_VALUE"}, true},
		{"GetEnvFlag returns false if env var is set to false", args{"FALSE_VALUE"}, false},
		{"GetEnvFlag returns false if env var is set to anything else", args{"FALSE_VALUE"}, false},
		{"GetEnvFlag returns false if env var is not set", args{"UNKNOWN_KEY"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetEnvFlag(tt.args.key); got != tt.want {
				t.Errorf("GetEnvFlag() = %v, want %v", got, tt.want)
			}
		})
	}
}
