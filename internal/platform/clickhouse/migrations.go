package clickhouse

import (
	"embed"
	"fmt"

	"github.com/pressly/goose/v3"
)

// Embed all the files in the migrations directory.
//
//go:embed migrations/*
var dbMigrations embed.FS

// MigrateDb runs the migrations for the clickhouse database.
func MigrateDb(clickHouseDsn string) error {
	db, err := goose.OpenDBWithDriver(driverName, clickHouseDsn)
	if err != nil {
		return fmt.Errorf("failed open CH connection: %w", err)
	}

	goose.SetBaseFS(dbMigrations)

	if err := goose.SetDialect(driverName); err != nil {
		return fmt.Errorf("failed to setup dialect for migration: %w", err)
	}
	if err := goose.Up(db, migrationsDir); err != nil {
		return fmt.Errorf("failed to run migrations: %w", err)
	}

	return nil
}

func RollbackDB(clickHouseDsn string) error {
	db, err := goose.OpenDBWithDriver(driverName, clickHouseDsn)
	if err != nil {
		return fmt.Errorf("failed open CH connection: %w", err)
	}

	goose.SetBaseFS(dbMigrations)

	if err := goose.SetDialect(driverName); err != nil {
		return fmt.Errorf("failed to setup dialect for migration: %w", err)
	}
	if err := goose.Down(db, migrationsDir); err != nil {
		return fmt.Errorf("failed to rollback migrations: %w", err)
	}

	return nil
}
