CREATE MATERIALIZED VIEW IF NOT EXISTS default.enriched_events_mv
TO default.snowplow_queue
AS
SELECT
    enriched_event[1] AS app_id,
    enriched_event[2] AS platform,
    enriched_event[3] AS etl_tstamp,
    enriched_event[4] AS collector_tstamp,
    enriched_event[5] AS dvce_created_tstamp,
    enriched_event[6] AS event,
    enriched_event[7] AS event_id,
    enriched_event[8] AS txn_id,
    enriched_event[9] AS name_tracker,
    enriched_event[10] AS v_tracker,
    enriched_event[11] AS v_collector,
    enriched_event[12] AS v_etl,
    enriched_event[13] AS user_id,
    enriched_event[14] AS user_ipaddress,
    enriched_event[15] AS user_fingerprint,
    enriched_event[16] AS domain_userid,
    enriched_event[17] AS domain_sessionidx,
    enriched_event[18] AS network_userid,
    enriched_event[19] AS geo_country,
    enriched_event[20] AS geo_region,
    enriched_event[21] AS geo_city,
    enriched_event[22] AS geo_zipcode,
    enriched_event[23] AS geo_latitude,
    enriched_event[24] AS geo_longitude,
    enriched_event[25] AS geo_region_name,
    enriched_event[26] AS ip_isp,
    enriched_event[27] AS ip_organization,
    enriched_event[28] AS ip_domain,
    enriched_event[29] AS ip_netspeed,
    enriched_event[30] AS page_url,
    enriched_event[31] AS page_title,
    enriched_event[32] AS page_referrer,
    enriched_event[33] AS page_urlscheme,
    enriched_event[34] AS page_urlhost,
    enriched_event[35] AS page_urlport,
    enriched_event[36] AS page_urlpath,
    enriched_event[37] AS page_urlquery,
    enriched_event[38] AS page_urlfragment,
    enriched_event[39] AS refr_urlscheme,
    enriched_event[40] AS refr_urlhost,
    enriched_event[41] AS refr_urlport,
    enriched_event[42] AS refr_urlpath,
    enriched_event[43] AS refr_urlquery,
    enriched_event[44] AS refr_urlfragment,
    enriched_event[45] AS refr_medium,
    enriched_event[46] AS refr_source,
    enriched_event[47] AS refr_term,
    enriched_event[48] AS mkt_medium,
    enriched_event[49] AS mkt_source,
    enriched_event[50] AS mkt_term,
    enriched_event[51] AS mkt_content,
    enriched_event[52] AS mkt_campaign,
    enriched_event[53] AS contexts,
    enriched_event[54] AS se_category,
    enriched_event[55] AS se_action,
    enriched_event[56] AS se_label,
    enriched_event[57] AS se_property,
    toDecimal32OrNull(enriched_event[58], 2) AS se_value,
    enriched_event[59] AS unstruct_event,
    enriched_event[60] AS tr_orderid,
    enriched_event[61] AS tr_affiliation,
    toDecimal32OrNull(enriched_event[62], 2) AS tr_total,
    toDecimal32OrNull(enriched_event[63], 2) AS tr_tax,
    toDecimal32OrNull(enriched_event[64], 2) AS tr_shipping,
    enriched_event[65] AS tr_city,
    enriched_event[66] AS tr_state,
    enriched_event[67] AS tr_country,
    enriched_event[68] AS ti_orderid,
    enriched_event[69] AS ti_sku,
    enriched_event[70] AS ti_name,
    enriched_event[71] AS ti_category,
    toDecimal32OrNull(enriched_event[72], 2) AS ti_price,
    toDecimal32OrNull(enriched_event[73], 2) AS ti_quantity,
    enriched_event[74] AS pp_xoffset_min,
    enriched_event[75] AS pp_xoffset_max,
    enriched_event[76] AS pp_yoffset_min,
    enriched_event[77] AS pp_yoffset_max,
    enriched_event[78] AS useragent,
    enriched_event[79] AS br_name,
    enriched_event[80] AS br_family,
    enriched_event[81] AS br_version,
    enriched_event[82] AS br_type,
    enriched_event[83] AS br_renderengine,
    enriched_event[84] AS br_lang,
    enriched_event[85] AS br_features_pdf,
    enriched_event[86] AS br_features_flash,
    enriched_event[87] AS br_features_java,
    enriched_event[88] AS br_features_director,
    enriched_event[89] AS br_features_quicktime,
    enriched_event[90] AS br_features_realplayer,
    enriched_event[91] AS br_features_windowsmedia,
    enriched_event[92] AS br_features_gears,
    enriched_event[93] AS br_features_silverlight,
    enriched_event[94] AS br_cookies,
    enriched_event[95] AS br_colordepth,
    enriched_event[96] AS br_viewwidth,
    enriched_event[97] AS br_viewheight,
    enriched_event[98] AS os_name,
    enriched_event[99] AS os_family,
    enriched_event[100] AS os_manufacturer,
    enriched_event[101] AS os_timezone,
    enriched_event[102] AS dvce_type,
    enriched_event[103] AS dvce_ismobile,
    enriched_event[104] AS dvce_screenwidth,
    enriched_event[105] AS dvce_screenheight,
    enriched_event[106] AS doc_charset,
    enriched_event[107] AS doc_width,
    enriched_event[108] AS doc_height,
    enriched_event[109] AS tr_currency,
    toDecimal32OrNull(enriched_event[110], 2) AS tr_total_base,
    toDecimal32OrNull(enriched_event[111], 2) AS tr_tax_base,
    toDecimal32OrNull(enriched_event[112], 2) AS tr_shipping_base,
    enriched_event[113] AS ti_currency,
    toDecimal32OrNull(enriched_event[114], 2) AS ti_price_base,
    enriched_event[115] AS base_currency,
    enriched_event[116] AS geo_timezone,
    enriched_event[117] AS mkt_clickid,
    enriched_event[118] AS mkt_network,
    enriched_event[119] AS etl_tags,
    enriched_event[120] AS dvce_sent_tstamp,
    enriched_event[121] AS refr_domain_userid,
    enriched_event[122] AS refr_device_tstamp,
    enriched_event[123] AS derived_contexts,
    enriched_event[124] AS domain_sessionid,
    enriched_event[125] AS derived_tstamp,
    enriched_event[126] AS event_vendor,
    enriched_event[127] AS event_name,
    enriched_event[128] AS event_format,
    enriched_event[129] AS event_version,
    enriched_event[130] AS event_fingerprint,
    enriched_event[131] AS true_tstamp
FROM
    (SELECT splitByChar('\t', message) as enriched_event FROM default.enriched_events);
