CREATE MATERIALIZED VIEW IF NOT EXISTS default.snowplow_bad_events_consumer
    TO default.snowplow_bad_events
AS
SELECT 
    bad_event[1] as schema,
    bad_event[2] as data,
    _timestamp as timestamp
FROM
    (SELECT splitByChar('\t', message) as bad_event, _timestamp FROM default.snowplow_bad_events_queue);
