CREATE TABLE IF NOT EXISTS {database_name:Identifier}.snowplow_events ON CLUSTER '{cluster}'
    AS {database_name:Identifier}.snowplow_events_local
    ENGINE = Distributed('{cluster}', {database_name:Identifier}, snowplow_events_local, rand());