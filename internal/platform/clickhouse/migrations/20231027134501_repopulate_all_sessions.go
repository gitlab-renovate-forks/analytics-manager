package migrations

import (
	"context"
	"database/sql"
	"log"
	"strings"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upRepopulateAllSessions, downRepopulateAllSessions)
}

func upRepopulateAllSessions(ctx context.Context, tx *sql.Tx) error {
	var projectIds []string

	hasActiveAppsTable, err := activeAppsTableExists(ctx, tx)
	switch {
	case err != nil:
		return err
	case !hasActiveAppsTable:
		return nil
	}

	rows, err := tx.QueryContext(ctx, "SELECT Distinct project_id from default.active_apps")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var projectId string
		if err := rows.Scan(&projectId); err != nil {
			return err
		}
		projectIds = append(projectIds, projectId)
	}

	for _, projectId := range projectIds {
		log.Printf("Moving on to project: %s", projectId)

		log.Printf("Renaming table...")
		// 01_rename_table.sql
		renameTableQuery, err := f.ReadFile("queries/migrate_all_sessions/01_rename_table.sql")
		if err != nil {
			log.Fatal(err)
		}
		renameTableQueryInterpolated := strings.ReplaceAll(string(renameTableQuery), "{database_name:Identifier}", projectId)

		_, err = tx.ExecContext(ctx, renameTableQueryInterpolated)
		if err != nil {
			return err
		}

		log.Printf("Creating new events table...")
		// 02_create_events_table.sql
		createEventsTableQuery, err := f.ReadFile("queries/migrate_all_sessions/02_create_events_table.sql")
		if err != nil {
			log.Fatal(err)
		}
		createEventsTableQueryInterpolated := strings.ReplaceAll(string(createEventsTableQuery), "{database_name:Identifier}", projectId)

		_, err = tx.ExecContext(ctx, createEventsTableQueryInterpolated)
		if err != nil {
			return err
		}

		log.Printf("Truncating sessions list...")
		// 03_truncate_old_sessions.sql
		truncateOldSessionsQuery, err := f.ReadFile("queries/migrate_all_sessions/03_truncate_old_sessions.sql")
		if err != nil {
			log.Fatal(err)
		}
		truncateOldSessionsQueryInterpolated := strings.ReplaceAll(string(truncateOldSessionsQuery), "{database_name:Identifier}", projectId)

		_, err = tx.ExecContext(ctx, truncateOldSessionsQueryInterpolated)
		if err != nil {
			return err
		}

		log.Printf("Copying data to new table... This may take a long time for large project. (gitlab-org/gitlab should take around 1 hour)")
		// 04_insert_into_snowplow_events.sql
		insertIntoSnowplowEvents, err := f.ReadFile("queries/migrate_all_sessions/04_insert_into_snowplow_events.sql")
		if err != nil {
			log.Fatal(err)
		}
		insertIntoSnowplowEventsInterpolated := strings.ReplaceAll(string(insertIntoSnowplowEvents), "{database_name:Identifier}", projectId)

		_, err = tx.ExecContext(ctx, insertIntoSnowplowEventsInterpolated)
		if err != nil {
			return err
		}
	}

	return nil
}

func downRepopulateAllSessions(ctx context.Context, tx *sql.Tx) error {
	// no-op. Do not delete existing sessions tables once migrated.
	return nil
}
