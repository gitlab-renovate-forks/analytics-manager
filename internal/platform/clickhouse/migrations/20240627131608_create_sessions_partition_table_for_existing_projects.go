package migrations

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/pressly/goose/v3"
	"log"
	"strings"
)

func init() {
	goose.AddMigrationContext(upCreateSessionsPartitionTableForExistingProjectsGo, downCreateSessionsPartitionTableForExistingProjectsGo)
}

func upCreateSessionsPartitionTableForExistingProjectsGo(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	projectIDs, err := getProjectIds(ctx, tx)
	if err != nil {
		log.Printf("Error fetching project ids from active apps")
		return err
	}

	for _, projectID := range projectIDs {
		log.Printf("Creating partition table for project: %s", projectID)

		partitionTableQuery, err := f.ReadFile("queries/partition_sessions_table/01_create_partitioned_sessions_table.sql")
		if err != nil {
			log.Fatal(err)
		}
		partitionTableQueryInterpolated := strings.ReplaceAll(string(partitionTableQuery), "{database_name:Identifier}", projectID)
		_, err = tx.ExecContext(ctx, partitionTableQueryInterpolated)
		if err != nil {
			return err
		}

		log.Printf("Creating sessions partition view for project: %s", projectID)

		partitionViewQuery, err := f.ReadFile("queries/partition_sessions_table/02_create_sessions_partition_view.sql")
		if err != nil {
			log.Fatal(err)
		}
		partitionViewQueryInterpolated := strings.ReplaceAll(string(partitionViewQuery), "{database_name:Identifier}", projectID)
		_, err = tx.ExecContext(ctx, partitionViewQueryInterpolated)
		if err != nil {
			return err
		}
	}
	return nil
}

func downCreateSessionsPartitionTableForExistingProjectsGo(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	projectIDs, err := getProjectIds(ctx, tx)
	if err != nil {
		log.Printf("Error fetching project ids from active apps")
		return err
	}

	for _, projectID := range projectIDs {
		log.Printf("Destroying partition table for project: %s", projectID)

		_, err = tx.ExecContext(ctx, fmt.Sprintf("DROP TABLE IF EXISTS %s.sessions_partition", projectID))
		if err != nil {
			return err
		}

		log.Printf("Destroying sessions partition materialized view for project: %s", projectID)

		_, err = tx.ExecContext(ctx, fmt.Sprintf("DROP VIEW IF EXISTS %s.sessions_partition_mv", projectID))
		if err != nil {
			return err
		}
	}
	return nil
}

func getProjectIds(ctx context.Context, tx *sql.Tx) ([]string, error) {
	var projectIDs []string
	hasActiveAppsTable, err := activeAppsTableExists(ctx, tx)
	switch {
	case err != nil:
		return projectIDs, err
	case !hasActiveAppsTable:
		return projectIDs, nil
	}

	rows, err := tx.QueryContext(ctx, "SELECT DISTINCT project_id from default.active_apps")
	if err != nil {
		return projectIDs, err
	}
	defer rows.Close()

	for rows.Next() {
		var projectID string
		if err := rows.Scan(&projectID); err != nil {
			return projectIDs, err
		}
		projectIDs = append(projectIDs, projectID)
	}
	return projectIDs, nil
}
