CREATE TABLE IF NOT EXISTS default.snowplow_bad_events_queue (
    message String,
    _timestamp DateTime DEFAULT now()
) ENGINE = Null
