# analytics-manager

This is the analytics-manager for the analytics-stack. It configures new projects for Snowplow event tracking by creating the necessary database and tables in Clickhouse.
Both endpoints are protected via HTTP Basic Auth 

## Environment

The service expects the following environment variables to be configured:

- `CLICKHOUSE_URL`: The url to the clickhouse instance that should be configured with protocol and port. Example: `http://clickhouse:8123`. Both `http` and `https` are supported.
- `CLICKHOUSE_USERNAME`: The username for the clickhouse instance that should be configured
- `CLICKHOUSE_PASSWORD`: The password for the clickhouse instance that should be configured
- `AUTH_USERNAME`: Username for HTTP-Basic auth with this service
- `AUTH_PASSWORD`: Password for HTTP-Basic auth with this service
- `CLUSTER_MODE_ENABLED`: Boolean flag to enable standalone cluster support for the project

## Project setup
```
POST /setup-project/:project_id

RESPONSE
{ app_id: [app_id_for_SDK], db_name: [project_id] }
```

This creates a new database called `project_id` and a table called `snowplow_events` within the database. It generates and returns a unique `app_id`.
Any event sent with this `app_id` will be automatically moved into the `[project_id].snowplow_events` database.

**Important**: Due to the equivalence between `project_id` and the name of the database right now the following
restrictions apply to the `project_id`:

*The `project_id` may contain Latin letters, numbers, and underscores. The maximum name length is 63 characters. You
can't use an `project_id` named default or one which is just a decimal number.*

Example request using curl:
```
curl --request POST --url https://[manager-domain]/setup-project/test  -H 'Content-Length: 0' -u username:password
```

## Database migration

The analytics-manager uses [goose](https://github.com/pressly/goose) for database migrations.

To migrate the database, run the following command:

```
CLICKHOUSE_PASSWORD=<password> CLICKHOUSE_URL=<clickhouse address> CLICKHOUSE_USERNAME=<username> go run ./cmd/migrate
```

The database migrations state is not performed automatically on the application start. The migration has to be executed manually.

To rollback the last applied migration on database, run the following command:

```
CLICKHOUSE_PASSWORD=<password> CLICKHOUSE_URL=<clickhouse address> CLICKHOUSE_USERNAME=<username> go run ./cmd/rollback
```

### Adding a new migration option 1: without installing goose locally

To add a new migration without installing goose locally, you can create a new `.sql`
text migration file in the `/internal/platform/clickhouse/migrations` folder. Please follow the migration naming convention
`[timestamp]_[migration_name].sql`.

Example:

```clickhouse
-- +goose Up
CREATE TABLE helloworld.my_first_table
(
   user_id   UInt32,
   message   String,
   timestamp DateTime,
   metric    Float32
)
   ENGINE = MergeTree()
      PRIMARY KEY (user_id, timestamp)

-- +goose Down
DROP TABLE helloworld.my_first_table;
```

### Adding a new migration option 2: with installing goose locally

1. Install [goose locally](https://github.com/pressly/goose#install)
2. Go to the `/internal/platform/clickhouse/migrations` folder
2. Create a new migration file with `goose create [migration_name] sql`
3. Edit the newly created migration file.

## Building the project

The project includes a dockerfile and can be built via `docker build . -t analytics_manager`

## Releasing a new version

To release a new version, create a new tag and push it to the repository (or create it in the project UI).
We use [SemVer](https://semver.org/) for versioning.

This will trigger a new build (`release_job` job) and the new version will be available in the container registry as
`gitlab-org/analytics-section/analytics-manager:[tag]`
and `gitlab-org/analytics-section/analytics-manager:latest`. [New release](https://gitlab.com/gitlab-org/analytics-section/analytics-manager/-/releases)
will also be created.

Every new release description should contain changelog entry listing all changes that were included in given release helping everyone understanding how every release are different from each other
Example changelog entry:

```
Changelog:

1. Added to /setup-kafka-clickhouse bad events stream integration from Kafka to ClickHouse !11
```

## Architecture

### Database tables

On initial setup, a table called `enriched_events` is created in the default database. From there, Vector will push events from Kafka into `enriched_events`, and the materialized view `enriched_events_mv` will be triggered to re-map and insert events into the `snowplow_queue` table, which is then queried upon to insert events into their namespaced databases dependent on their given application ID.

Whenever a project is setup these additional steps happen:

1. A new database with `db_name=project_id` is set up.
2. Inside the new database:
  1. A table called `snowplow_events` is created, to house all the events for this project.
  2. A materialized_view called `snowplow_consumer` is created which will move the events for the returned `app_id` from `default.snowplow_queue` into `[project_id].snowplow_events`. This view will also transform the original data structure from Snowplow into something more suitable for us.

## How To

### Create new denormalized column to extract data from JSON blobs

In Snowplow most custom data is stored either in the `contexts`, `unstruct_event` or `event_properties` column. These are JSON columns which are very flexible but can be hard and slow to query.
As a result, we chose to denormalize data that is important for repeated queries into separate columns. This works the following way:

**Disclaimer:** This will currently only automatically fill the new column for newly created projects. We're working on a way to migrate existing projects.

1. Add the new column in the [snowplow events table](internal/platform/clickhouse/queries/create_project_snowplow_events_table.sql)
2. Modify the [snowplow consumer view] move incoming data into the new column. Depending on which data you want to move this can be done in one of the following ways:
  1. To extract a specific property from an event use an appropriate function from the [Clickhouse JSON functions](https://clickhouse.com/docs/en/sql-reference/functions/json-functions#visitparamextractstringparams-name)
  1. To extract a specific context into a separate column you can use our custom function: `extract_schema_data([contexts|derived_contexts], '[context_name]')`

Examples for this can be found in the [create_project_view.sql](internal/platform/clickhouse/queries/create_project_view.sql), for `custom_event_name`, `custom_event_props` and `custom_user_props`.

# Development

## Set up Clickhouse with HTTPS support locally

To test SSL encrypted communication between the analytics manager and Clickhouse you would need to set up a local Clickhouse
instance with HTTPS enabled:

1. Install Clickhouse locally following
   the [official instructions](https://clickhouse.com/docs/en/install#quick-install)
2. Create a self-signed certificate and key for
   Clickhouse. [Example guide](https://clickhouse.com/docs/en/guides/sre/configuring-ssl#2-create-ssl-certicates)
3. Specify the generated keys in custom `config.xml` file. [Example config](./config-example.xml)
4. Start the server with the custom config: `./clickhouse-server --config-file=config-example.xml`
