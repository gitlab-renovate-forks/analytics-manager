package tests

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/mock"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/analytics-section/analytics-manager/cmd/servid/handlers"
)

type mockedDB struct {
	mock.Mock
}

func (m *mockedDB) ProjectUsageByMonth(projectId string, year int, month int) (int, error) {
	args := m.Called(projectId, year, month)
	return args.Int(0), args.Error(1)

}

func (m *mockedDB) CreateDatabase(databaseName string) error {
	args := m.Called(databaseName)
	return args.Error(0)
}

func (m *mockedDB) CreateProjectSnowplowEventsTable(projectId string, isCluster bool) error {
	args := m.Called(projectId, isCluster)
	return args.Error(0)
}

func (m *mockedDB) GetAppIdByProjectId(projectId string) (*string, error) {
	args := m.Called(projectId)
	return args.Get(0).(*string), args.Error(1)
}

func (m *mockedDB) InsertIntoActiveApps(appId string, projectId string) error {
	args := m.Called(appId, projectId)
	return args.Error(0)
}

func (m *mockedDB) CreateProjectView(appId string, projectId string) error {
	args := m.Called(appId, projectId)
	return args.Error(0)
}

func (m *mockedDB) CreateSessionsTable(projectId string) error {
	args := m.Called(projectId)
	return args.Error(0)
}

func (m *mockedDB) CreatePartitionedSessionsTable(projectID string) error {
	args := m.Called(projectID)
	return args.Error(0)
}

func (m *mockedDB) CreatePartitionedSessionsView(projectID string) error {
	args := m.Called(projectID)
	return args.Error(0)
}

func (m *mockedDB) CreateSessionsView(projectId string) error {
	args := m.Called(projectId)
	return args.Error(0)
}

func (m *mockedDB) CleanDefaultDb() ([]string, error) {
	args := m.Called()
	return args.Get(0).([]string), args.Error(1)
}

func (m *mockedDB) GetAllProjectDBNames() ([]string, error) {
	args := m.Called()
	return args.Get(0).([]string), args.Error(1)
}

func (m *mockedDB) GetPendingMigrations() (int, error) {
	args := m.Called()
	return args.Int(0), args.Error(1)
}

func mockCreateQuery(testDB *mockedDB, entity string, entityName string) {
	matcherFunction := requestWithPrefix(fmt.Sprintf("CREATE %s IF NOT EXISTS %s", entity, entityName))
	testDB.On("Exec", mock.MatchedBy(matcherFunction)).Return(nil, nil)
}

func requestWithPrefix(prefix string) func(req string) bool {
	return func(req string) bool {
		return strings.HasPrefix(strings.TrimSpace(req), prefix)
	}
}

func (m *mockedDB) CreateFunnelSchemasTable(projectId string) error {
	args := m.Called(projectId)
	return args.Error(0)
}

func (m *mockedDB) InsertFunnelSchema(projectID string, name string, contents string) error {
	args := m.Called(projectID, name, contents)
	return args.Error(0)
}

func (m *mockedDB) DeleteFunnelSchema(projectID string, name string) error {
	args := m.Called(projectID, name)
	return args.Error(0)
}

func setupTestServerWithDB() (*handlers.Server, *mockedDB) {
	_ = godotenv.Load("../.env.test")
	testDB := new(mockedDB)
	srv := &handlers.Server{DB: testDB}
	srv.InitServer()

	return srv, testDB
}

func TestMain(m *testing.M) {
	// Switch to test mode, no noisy output from Gin
	_ = godotenv.Load(".env.test")
	gin.SetMode(gin.TestMode)

	code := m.Run()
	os.Exit(code)
}

func TestHealthz(t *testing.T) {
	srv, _ := setupTestServerWithDB()

	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/healthz", nil)
	srv.Gin.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)
}
