package tests

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestSetupProject(t *testing.T) {
	projectName := "abc123"

	srv, testDB := setupTestServerWithDB()

	testDB.On("CreateDatabase", projectName).Return(nil)
	testDB.On("CreateProjectSnowplowEventsTable", projectName, false).Return(nil)
	testDB.On("CreateSessionsTable", projectName).Return(nil)
	testDB.On("CreatePartitionedSessionsTable", projectName).Return(nil)
	testDB.On("CreatePartitionedSessionsView", projectName).Return(nil)
	testDB.On("CreateSessionsView", projectName).Return(nil)
	testDB.On("CreateFunnelSchemasTable", projectName).Return(nil)
	var appID *string
	testDB.On("GetAppIdByProjectId", projectName).Return(appID, nil)
	testDB.On("InsertIntoActiveApps", mock.AnythingOfType("string"), projectName).Return(nil)
	testDB.On("CreateProjectView", mock.AnythingOfType("string"), projectName).Return(nil)

	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", fmt.Sprintf("/setup-project/%s", projectName), nil)
	req.SetBasicAuth("user", "pass")
	srv.Gin.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	testDB.AssertExpectations(t)
}

func TestSetupProjectWhenProjectExistsReturns200(t *testing.T) {
	projectName := "abc123"

	srv, testDB := setupTestServerWithDB()

	testDB.On("CreateDatabase", projectName).Return(nil)
	testDB.On("CreateProjectSnowplowEventsTable", projectName, false).Return(nil)
	testDB.On("CreateSessionsTable", projectName).Return(nil)
	testDB.On("CreatePartitionedSessionsTable", projectName).Return(nil)
	testDB.On("CreatePartitionedSessionsView", projectName).Return(nil)
	testDB.On("CreateSessionsView", projectName).Return(nil)
	testDB.On("CreateFunnelSchemasTable", projectName).Return(nil)
	appID := "app"
	testDB.On("GetAppIdByProjectId", projectName).Return(&appID, nil)

	testDB.AssertNotCalled(t, "InsertIntoActiveApps", appID, projectName)

	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", fmt.Sprintf("/setup-project/%s", projectName), nil)
	req.SetBasicAuth("user", "pass")
	srv.Gin.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	testDB.AssertExpectations(t)
}

func TestSetupProjectReturns500IfQueryFails(t *testing.T) {
	projectName := "abc123"

	srv, testDB := setupTestServerWithDB()

	testDB.On("CreateDatabase", projectName).Return(fmt.Errorf("DB creation fail"))

	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", fmt.Sprintf("/setup-project/%s", projectName), nil)
	req.SetBasicAuth("user", "pass")
	srv.Gin.ServeHTTP(resp, req)

	assert.Equal(t, 500, resp.Code)

	testDB.AssertExpectations(t)
}
