package handlers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"
)

// ProjectUsage returns useful information about a project's data usage
func (s *Server) ProjectUsage(ctx *gin.Context) {
	projectID := ctx.Param("projectId")
	month, monthErr := strconv.Atoi(ctx.Param("month"))
	year, yearErr := strconv.Atoi(ctx.Param("year"))
	if !utils.StartsWithLetter(projectID) {
		projectID = "db_" + projectID
	}

	if monthErr != nil {
		ctx.JSON(http.StatusBadRequest, "could not parse month")
		return
	}

	if yearErr != nil {
		ctx.JSON(http.StatusBadRequest, "could not parse year")
		return
	}

	result, err := s.DB.ProjectUsageByMonth(projectID, year, month)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"result":     result,
		"period":     gin.H{"month": month, "year": year},
		"project_id": projectID,
	})
}
