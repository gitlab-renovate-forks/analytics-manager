// Package handlers defines Server setup, routing and request handlers
package handlers

import (
	"fmt"
	"net/http"

	_ "github.com/ClickHouse/clickhouse-go/v2" // clickhouse driver import
	"github.com/gin-gonic/gin"

	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"
)

type authConfig struct {
	username string
	password string
}

type config struct {
	auth      authConfig
	isCluster bool
}

func serverConfig() *config {
	return &config{
		auth: authConfig{
			username: utils.GetEnv("AUTH_USERNAME"),
			password: utils.GetEnv("AUTH_PASSWORD"),
		},
		isCluster: utils.GetEnvFlag("CLUSTER_MODE_ENABLED"),
	}
}

type Database interface {
	ProjectUsageByMonth(projectId string, year int, month int) (int, error)
	CreateDatabase(databaseName string) error
	CreateProjectSnowplowEventsTable(projectId string, isCluster bool) error
	GetAppIdByProjectId(projectId string) (*string, error)
	InsertIntoActiveApps(appId string, projectId string) error
	CreateProjectView(appId string, projectId string) error
	CreateSessionsTable(projectId string) error
	CreatePartitionedSessionsTable(projectID string) error
	CreatePartitionedSessionsView(projectID string) error
	CreateSessionsView(projectId string) error
	CleanDefaultDb() ([]string, error)
	GetAllProjectDBNames() ([]string, error)
	GetPendingMigrations() (int, error)
	CreateFunnelSchemasTable(projectId string) error
	InsertFunnelSchema(projectID string, name string, contents string) error
	DeleteFunnelSchema(projectID string, name string) error
}

type Server struct {
	DB     Database
	Gin    *gin.Engine
	Config *config
}

func (s *Server) InitServer() *Server {
	g := gin.Default()
	g.Use(gin.Recovery())

	s.Gin = g
	s.Config = serverConfig()
	s.RegisterRoutes()

	return s
}

func (s *Server) Ready() bool {
	return s.DB != nil && s.Gin != nil
}

func (s *Server) Start(ep string) error {
	if !s.Ready() {
		return fmt.Errorf("server isn't ready - make sure to init db and gin")
	}

	if err := http.ListenAndServe(ep, s.Gin.Handler()); err != nil {
		return err
	}

	return nil
}
